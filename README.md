# Audrey Vincent's - README.md
## About Me
My name is Audrey Vincent and I'm a customer focused digital product manager with 5 years of experience and a background in user experience design, software engineering, and entrepreneurship.
  - [LinkedIn Profile](https://www.linkedin.com/in/audreymariev/) and [Resume](https://audreyvincent.me/audrey-resume.pdf)
- Avid trail runner and gravel biker living in the front range of Colorado right outside of Boulder, CO.
- I'm a computer science grad from [Purdue University](https://www.purdue.edu/) with a minor in Art and Design and a certificate in Entrepreneurship.
- I did a semester exchange at [KTH Royal Institute of Technology](https://www.kth.se/en), Sweden's largest technical university where I studied master's level Computer Science and Human-Computer Interaction design courses.
- Passionate about designing for the customer, helping others navigate the outdoors, and creating a more sustainable world.

### Short-term Career Goals
- Transition to a technical hybrid product role that includes full stack web or iOS engineering at a 25-50 person startup in the energy industry.

## Working with me

### What I love
- Passionate about great user experiences.
- Defining the “why”.
- Brainstorming with others.
- Product discovery, building roadmap decks, defining personas/problems, writing user manuals, and monitoring feedback/metrics/user journeys.
- Listening to customer research and supporting design to quickly prototype, test, and validate before building.
- Defining product design processes with UX, data, and engineering.
- Looking at metrics, querying data myself, or working with analysts for insights.

### What I dislike
- Solutioning by myself.
- Inefficient communication (meetings with no agenda, email chains).
- Teammates that disrespect boundaries (for ex: fires on Friday after 5).
- Setting up meetings to get a response.
- Manual operations tasks not incorporated into the product. 
- Expectation to document implementation details rather than focusing on the “why”.
- Expectation to do on-call eng tasks (firefighting and troubleshooting vs. proactive testing).

## Long-term Career Goals
- Be the 1st or 2nd product hire at an early-stage startup, or start a company.
- Eventually run a small business (agency, art studio, subscription product) or start/work/volunteer at a non-profit.
- Lean into my passions in some capacity either professionally or in a volunteer role: work on outdoor map products, DERMs/electrification/energy decarbonization company, or genetic health/fertility company.

## Life Goals
- [x] Climb Long's Peak 14er
- [x] Train for first 35K ultra trail run this fall - [Crested Butte Ultra](https://www.madmooseevents.com/crested-butte-ultra)
- [x] Learn how to throw pottery
- [ ] Learn Go and Swift
- [ ] Become a trained Electrician
- [ ] Train and complete a duathlon

## Personality tests
- My Myers-Briggs is [Logician (INTP-T)](https://www.16personalities.com/intp-personality?utm_source=welcome-turbulent-logician&utm_medium=email&utm_campaign=results&utm_content=type-personality-0)
